package com.example.setgame

class CardsResponse(
    val status: String,
    val board: Collection<Card>
)