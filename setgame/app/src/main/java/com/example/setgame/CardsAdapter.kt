package com.example.setgame

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView

class CardsAdapter(private val list: List<Card>) : RecyclerView.Adapter<CardsAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val cardIdTextView: TextView
        private val cardCountTextView: TextView
        private val cardColorTextView: TextView
        private val cardShapeTextView: TextView
        private val cardFillTextView: TextView
        private val cardImageView: ImageView

        init {
            cardIdTextView = itemView.findViewById(R.id.card_id)
            cardCountTextView = itemView.findViewById(R.id.card_count)
            cardColorTextView = itemView.findViewById(R.id.card_color)
            cardShapeTextView = itemView.findViewById(R.id.card_shape)
            cardFillTextView = itemView.findViewById(R.id.card_fill)
            cardImageView = itemView.findViewById(R.id.card_image)

        }

        fun bindTo(card: Card) {
            card.apply {
//                cardIdTextView.text = "id: (${id})"
//                cardCountTextView.text = "count: (${count})"
//                cardColorTextView.text = "color: (${color})"
//                cardShapeTextView.text = "shape: (${shape})"
//                cardFillTextView.text = "fill: (${fill})"
                cardImageView.setImageResource(getImageResource(count, color, shape, fill))
            }
        }
    }
    private fun getImageResource(count: Int, color: Int, shape: Int, fill: Int): Int {

        return when {
            color == 1 && shape == 1 && fill == 2 && count == 2 -> R.drawable.card1
            color == 1 && shape == 2 && fill == 1 && count == 3 -> R.drawable.card2
            color == 3 && shape == 3 && fill == 2 && count == 3 -> R.drawable.card3
            color == 1 && shape == 1 && fill == 2 && count == 3 -> R.drawable.card4
            color == 1 && shape == 2 && fill == 2 && count == 1 -> R.drawable.card5
            color == 3 && shape == 3 && fill == 2 && count == 1 -> R.drawable.card6
            color == 1 && shape == 1 && fill == 2 && count == 1 -> R.drawable.card7
            color == 2 && shape == 2 && fill == 3 && count == 2 -> R.drawable.card8
            color == 2 && shape == 3 && fill == 1 && count == 2 -> R.drawable.card9
            color == 2 && shape == 2 && fill == 1 && count == 1 -> R.drawable.card10
            color == 3 && shape == 3 && fill == 1 && count == 2 -> R.drawable.card11
            color == 1 && shape == 1 && fill == 3 && count == 1 -> R.drawable.card12

            else -> R.drawable.ic_launcher_foreground
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.bindTo(list[pos])
    }

}