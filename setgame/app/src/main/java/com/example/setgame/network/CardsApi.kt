package com.example.setgame.network

import com.example.setgame.CardsResponse
import retrofit2.http.GET

interface CardsApi {
    @GET("/cards")
    suspend fun getAll(): CardsResponse
}