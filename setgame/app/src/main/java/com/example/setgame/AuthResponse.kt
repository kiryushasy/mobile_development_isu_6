package com.example.setgame

data class AuthResponse (
    val status: String,
    val token: String,
)