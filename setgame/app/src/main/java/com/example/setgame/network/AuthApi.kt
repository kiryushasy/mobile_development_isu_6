package com.example.setgame.network

import com.example.setgame.AuthBody
import com.example.setgame.AuthResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {
    @POST("/registration")
    suspend fun register(@Body body: AuthBody): AuthResponse
}