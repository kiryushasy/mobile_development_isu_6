package com.example.setgame

data class Card (
    val id: Int,
    val count: Int,
    val color: Int,
    val shape: Int,
    val fill: Int
    )