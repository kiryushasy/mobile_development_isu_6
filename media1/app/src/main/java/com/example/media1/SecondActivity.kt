package com.example.media1


import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import java.io.IOException


class SecondActivity : AppCompatActivity() {
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var playPauseButton: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var progressTextView: TextView
    private var isPlaying = false
    private var totalDuration = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        mediaPlayer = MediaPlayer()
        try {
            assets.openFd("ppos.mp3").use { fd ->
                mediaPlayer.setDataSource(fd.fileDescriptor, fd.startOffset, fd.length)
            }
            mediaPlayer.prepare()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        playPauseButton = findViewById(R.id.playPauseButton)
        progressBar = findViewById(R.id.progressBar)
        progressTextView = findViewById(R.id.progressTextView)

        totalDuration = mediaPlayer.duration
        progressBar.max = totalDuration

        playPauseButton.setOnClickListener {
            if (isPlaying) {
                mediaPlayer.pause()
                isPlaying = false
                playPauseButton.text = "Play"
            } else {
                mediaPlayer.start()
                isPlaying = true
                playPauseButton.text = "Pause"
            }
        }

        updateProgressBar()
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.release()
    }

    override fun onPause() {
        super.onPause()
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
            isPlaying = false
            playPauseButton.text = "Play"
        }
    }

    private fun updateProgressBar() {
        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                val currentPosition = mediaPlayer.currentPosition
                progressBar.progress = currentPosition
                progressTextView.text = formatDuration(currentPosition) + "/" + formatDuration(totalDuration)
                handler.postDelayed(this, 1000)
            }
        }, 0)
    }

    private fun formatDuration(duration: Int): String {
        val minutes = duration / 1000 / 60
        val seconds = duration / 1000 % 60
        return String.format("%02d:%02d", minutes, seconds)
    }
}