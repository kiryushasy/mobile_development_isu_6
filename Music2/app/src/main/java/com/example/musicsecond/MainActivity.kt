package com.example.musicsecond

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.musicsecond.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var counterTrack = 1
    private val trackList = arrayOf("ppos.mp3")

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        MyMediaPlayerService.startService(applicationContext)
        MyMediaPlayerService.TRACK_NAME.value = trackList[0]

        binding.trackName.text = MyMediaPlayerService.TRACK_NAME.value

        binding.playOrStop.setOnClickListener(playOrStopButtonListener)


        binding.toSecondActivity.setOnClickListener(toSecondActivityButtonListener)
    }

    private val playOrStopButtonListener = { view: View ->
        MyMediaPlayerService.isPlaying.value = MyMediaPlayerService.isPlaying.value != true

        updatePlayOrStopButton()
    }

    override fun onResume() {
        super.onResume()

        updatePlayOrStopButton()
    }


    private val toSecondActivityButtonListener = { view: View ->
        startActivity(Intent(this, DetailActivity::class.java))
    }

    private fun updatePlayOrStopButton() {
        if (MyMediaPlayerService.isPlaying.value == true)
            binding.playOrStop.setImageResource(R.drawable.pause)
        else
            binding.playOrStop.setImageResource(R.drawable.play)
    }

    override fun onDestroy() {
        super.onDestroy()

        MyMediaPlayerService.stopService(applicationContext)
    }

}