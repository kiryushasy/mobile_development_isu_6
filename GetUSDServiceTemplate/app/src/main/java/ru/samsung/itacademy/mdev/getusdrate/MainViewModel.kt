package ru.samsung.itacademy.mdev.getusdrate

import android.location.Location
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    val rubRate = MutableLiveData<String>()
    val rateCheckInteractor = RateCheckInteractor()

    fun onCreate() {
        refreshRate()
    }

    fun onRefreshClicked() {
        refreshRate()
    }

    private fun refreshRate() {
        GlobalScope.launch(Dispatchers.Main) {
            val rate = rateCheckInteractor.requestRate()
            Log.d(TAG, "rubRate = $rate")
            rubRate.value = rate
        }
    }

    companion object {
        const val TAG = "MainViewModel"
        const val RUB_RATE_URL = "https://min-api.cryptocompare.com/data/pricemulti?fsyms=USD&tsyms=RUB&api_key={c3a79076e871b4aca82c5d03c7dfa436b4d416a2742d3db929a26709c99bc7f8}"
    }
}