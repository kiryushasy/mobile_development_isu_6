package com.example.ships

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.ships.databinding.ActivityMainBinding
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.generateButton.setOnClickListener {
            val board = generateBoard()
            binding.boardText.text = boardToString(board)
        }
    }

    private fun generateBoard(): Array<IntArray> {
        val board = Array(10) { IntArray(10) }
        val ships = listOf(4, 3, 3, 2, 2, 2, 1, 1, 1, 1)

        for (ship in ships) {
            var placed = false
            while (!placed) {
                val row = Random.nextInt(10)
                val col = Random.nextInt(10)
                val direction = Random.nextInt(2)

                placed = tryPlaceShip(board, ship, row, col, direction)
            }
        }

        return board
    }

    private fun tryPlaceShip(board: Array<IntArray>, ship: Int, row: Int, col: Int, direction: Int): Boolean {
        if (direction == 0) {
            if (col + ship > 10) return false
            for (i in 0 until ship) {
                if (!isCellFree(board, row, col + i)) return false
            }
            for (i in 0 until ship) {
                setCellOccupied(board, row, col + i)
            }
        } else {
            if (row + ship > 10) return false
            for (i in 0 until ship) {
                if (!isCellFree(board, row + i, col)) return false
            }
            for (i in 0 until ship) {
                setCellOccupied(board, row + i, col)
            }
        }

        return true
    }

    private fun isCellFree(board: Array<IntArray>, row: Int, col: Int): Boolean {
        for (i in -1..1) {
            for (j in -1..1) {
                val newRow = row + i
                val newCol = col + j
                if (newRow in 0..9 && newCol in 0..9 && board[newRow][newCol] == 1) {
                    return false
                }
            }
        }
        return true
    }

    private fun setCellOccupied(board: Array<IntArray>, row: Int, col: Int) {
        board[row][col] = 1
    }

    private fun boardToString(board: Array<IntArray>): String {
        val stringBuilder = StringBuilder()
        for (row in board) {
            for (cell in row) {
                stringBuilder.append(cell)
            }
            stringBuilder.append("\n")
        }
        return stringBuilder.toString()
    }

}
