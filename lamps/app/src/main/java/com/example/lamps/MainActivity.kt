package com.example.lamps

import android.os.Bundle
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private lateinit var gameBoard: Array<Array<ImageView>>
    private lateinit var gameState: Array<Array<Boolean>>
    private lateinit var scoreTextView: TextView
    private var score: Int = 0
    private var level: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        scoreTextView = findViewById(R.id.scoreTextView)

        createGameBoard(3)
        setGameBoardOnClickListener()
    }

    private fun createGameBoard(size: Int) {
        gameBoard = Array(size) { row ->
            Array(size) { col ->
                val imageView = ImageView(this)
                val sizeInPixels = resources.getDimensionPixelSize(R.dimen.circle_size)
                imageView.layoutParams = ViewGroup.LayoutParams(sizeInPixels, sizeInPixels)
                imageView.setImageResource(if (Random.nextBoolean()) R.drawable.filled_circle else R.drawable.outline_circle)
                imageView
            }
        }

        val gridLayout = findViewById<androidx.gridlayout.widget.GridLayout>(R.id.gridLayout)
        gridLayout.removeAllViews()
        for (row in gameBoard) {
            for (imageView in row) {
                gridLayout.addView(imageView)
            }
        }

        gameState = Array(size) { Array(size) { false } }
        gameState[0][0] = true
    }

    private fun setGameBoardOnClickListener() {
        for (row in gameBoard.indices) {
            for (col in gameBoard[row].indices) {
                gameBoard[row][col].setOnClickListener {
                    toggleState(row, col)
                    updateScore()
                    if (checkWin()) {
                        level++
                        createGameBoard(level + 1)
                        setGameBoardOnClickListener()
                    }
                }
            }
        }
    }

    private fun toggleState(row: Int, col: Int) {
        gameState[row][col] = !gameState[row][col]
        gameBoard[row][col].setImageResource(if (gameState[row][col]) R.drawable.filled_circle else R.drawable.outline_circle)

        for (i in gameState.indices) {
            gameState[i][col] = !gameState[i][col]
            gameBoard[i][col].setImageResource(if (gameState[i][col]) R.drawable.filled_circle else R.drawable.outline_circle)
        }

        for (j in gameState[row].indices) {
            gameState[row][j] = !gameState[row][j]
            gameBoard[row][j].setImageResource(if (gameState[row][j]) R.drawable.filled_circle else R.drawable.outline_circle)
        }
    }

    private fun checkWin(): Boolean {
        val firstState = gameState[0][0]
        for (row in gameState.indices) {
            for (col in gameState[row].indices) {
                if (gameState[row][col] != firstState) {
                    return false
                }
            }
        }
        return true
    }

    private fun updateScore() {
        score++
        scoreTextView.text = "Score: $score"
    }
}