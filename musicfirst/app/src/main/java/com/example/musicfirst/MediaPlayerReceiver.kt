package com.example.musicfirst

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class MediaPlayerReceiver : BroadcastReceiver() {
    private val liveDataDuration by lazy { MutableLiveData<String>("") }
    fun getLiveDataDuration(): LiveData<String> {
        return liveDataDuration
    }

    private val liveDataNowPosition by lazy { MutableLiveData<String>("") }
    fun getLiveDataNowPosition(): LiveData<String> {
        return liveDataNowPosition
    }

    override fun onReceive(context: Context, intent: Intent) {

        when (intent.getStringExtra(CONSTANTS.STRING_DATA_TYPE_KEY.value)) {
            CONSTANTS.STRING_DATA_DURATION_KEY.value -> {

                liveDataDuration.value = intent.getStringExtra(CONSTANTS.DURATION_TRACK.value)
            }
            CONSTANTS.NOW_POSITION_TRACK_KEY.value -> {

                liveDataNowPosition.value = intent.getStringExtra(CONSTANTS.NOW_POSITION_TRACK.value)
            }
        }
    }
}