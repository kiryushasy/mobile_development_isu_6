package com.example.listview

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException


class MainActivity : AppCompatActivity() {
    private lateinit var adapter: UserListAdapter
    private lateinit var listView: ListView
    private val users = ArrayList<User>()

    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listView = findViewById(R.id.list)
        val jsonString = getJsonDataFromAsset(this, "contacts.json")
        val contacts = JSONObject(jsonString.toString()).getJSONArray("contacts") as JSONArray
        for (i in 0 until  contacts.length()){
            users.add(User(contacts.getJSONObject(i).getString("name"), contacts.getJSONObject(i).getString("phone"), contacts.getJSONObject(i).getString("sex")))
        }
        adapter = UserListAdapter(this, users)
        listView.adapter = adapter
    }

    fun sort(view: View) {
        when(view.id){
            R.id.sort_sex -> {
                users.sortWith(compareBy {
                    it.sex
                })
            }
            R.id.sort_name -> {
                users.sortWith(compareBy {
                    it.name
                })
            }
            R.id.sort_tel -> {
                users.sortWith(compareBy {
                    it.phoneNumber
                })
            }
        }
        adapter.notifyDataSetInvalidated()
    }
}