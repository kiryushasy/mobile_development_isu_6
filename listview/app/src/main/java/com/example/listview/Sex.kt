package com.example.listview

enum class Sex {
    MAN, WOMAN, UNKNOWN
}