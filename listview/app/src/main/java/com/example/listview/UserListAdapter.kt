package com.example.listview

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import java.util.*
import kotlin.collections.ArrayList


class UserListAdapter(ctx: Context, users: ArrayList<User>) : BaseAdapter() {
    var ctx: Context
    var users: ArrayList<User>

    // TODO: реализовать сортировку по каждому из полей
    // класса: sex, name, phoneNumber
    init {
        this.ctx = ctx
        this.users = users
    }

    override fun getCount(): Int {
        return users.size
    }

    override fun getItem(position: Int): Any {
        return users[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        // получаем данные из коллекции
        var convertView = convertView
        val begin = Date()
        val u = users[position]

        // создаём разметку (контейнер)
        convertView = LayoutInflater.from(ctx).inflate(R.layout.useritem, parent, false)
        // получаем ссылки на элементы интерфейса
        val ivUserpic = convertView.findViewById<ImageView>(R.id.userpic)
        ivUserpic.setOnClickListener { v -> v.setBackgroundColor(Color.RED) }
        val tvName = convertView.findViewById<TextView>(R.id.name)
        val tvPhone = convertView.findViewById<TextView>(R.id.phone)

        // задаём содержание
        tvName.text = u.name
        tvPhone.text = u.phoneNumber
        when (u.sex) {
            Sex.MAN -> ivUserpic.setImageResource(R.drawable.user_man)
            Sex.WOMAN -> ivUserpic.setImageResource(R.drawable.user_woman)
            Sex.UNKNOWN -> ivUserpic.setImageResource(R.drawable.user_unknown)
        }
        val finish = Date()
        Log.d("mytag", "getView time: " + (finish.getTime() - begin.getTime()))
        return convertView
    }
}